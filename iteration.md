for (var i = 0; i < 4; i++) {
  setTimeout (() => {
     console.log(`Iteration #${i}`);
  }, 1000);
}

Menurut Saya karena di proses satu kali menggunakan setTimeout, atau karena tidak didefinisikan nilai i pada setTimeout sehingga nilai i tidak di proses.
kode yang benar adalah sebagai berikut :

for (var i = 0; i < 4; i++) {
  setTimeout ((i) => {
     console.log(`Iteration #${i}`);
  }, 1000, i);
}