a.
- tb_keluarga
  id_keluarga int(5),
  nm_keluarga varchar(30),
  kota_asal varchar(30),

- tb_silsilah
  id_silsilah varchar(5),
  generasi enum('1','2','3','4','5'),
  id_keluarga

b. Kekurangan :
- Belum emiliki Primary Key
   
   Kelebihan :
   - Terstruktur
   - Memiliki Relasi, sudah terhubung antar tabel.

c. SELECT a.*, b.* FROM tb_keluarga a, tb_silsilah b WHERE a.id_keluarga = b.id_keluarga;
